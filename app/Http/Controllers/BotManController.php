<?php

namespace App\Http\Controllers;

use App\Conversations\SolicitudInicial;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Middleware\ApiAi;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;
use Illuminate\Support\Facades\DB;

class BotManController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');
        $botman->listen();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }

    public function nlpDefaultReply(BotMan $bot)
    {
        $bot->types();
        $extras = $bot->getMessage()->getExtras();
        $apiReply = $extras['apiReply'];
        $apiAction = $extras['apiAction'];
        $apiIntent = $extras['apiIntent'];
        $bot->reply($apiReply);
    }

    public function solicitudInicial(BotMan $bot)
    {
        $bot->startConversation(new SolicitudInicial());
    }
}
