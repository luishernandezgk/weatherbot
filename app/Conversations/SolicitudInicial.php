<?php
/**
 * Created by PhpStorm.
 * User: Luis Hernandez
 * Date: 19/09/2018
 * Time: 03:21 PM
 */

namespace App\Conversations;


use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class SolicitudInicial extends Conversation
{

    /**
     * Primera pregunta para redirigir a la convesacion adecuada
     */
    public function askPeticion()
    {
        $question = Question::create("Que quiere hacer?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_peticion')
            ->addButtons([
                Button::create('Realizar un raclamo')->value('reclamo'),
                Button::create('Informarme sobre algo')->value('informacion'),
                Button::create('Realizar una sugerencia')->value('sugerencia')
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                switch ($answer->getValue()) {
                    case 'reclamo':
                        $this->say("Elegiste el reclamo");
                        $this->bot->startConversation(new Reclamo());
                        break;
                    case 'informacion':
                        $this->say("Elegiste el informacion");
                        break;
                    case 'sugerencia':
                        $this->say("Elegiste el sugerencia");
                        break;
                }
            }
        });
    }

    /**
     * @return mixed
     */
    public function run()
    {
        // TODO: Implement run() method.
        $this->askPeticion();
    }
}