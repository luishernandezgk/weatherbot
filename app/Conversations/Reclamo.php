<?php
/**
 * Created by PhpStorm.
 * User: Luis Hernandez
 * Date: 19/09/2018
 * Time: 03:22 PM
 */

namespace App\Conversations;


use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;

class Reclamo extends Conversation
{

    protected $reclamo;


    public function askReclamo()
    {
        $this->ask('Hola me puede indicar su raclamo?', function(Answer $answer) {
            // Save result
            $this->reclamo = $answer->getText();

            $this->say('Gracias por indicarnos su reclamo, trasmitire la sigueinte informacion a un operador: '.$this->reclamo);
        });
    }

    /**
     * @return mixed
     */
    public function run()
    {
        // TODO: Implement run() method.

        $this->askReclamo();

    }
}