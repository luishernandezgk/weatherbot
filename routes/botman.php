<?php
use App\Http\Controllers\BotManController;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Middleware\ApiAi;
use BotMan\Drivers\Web\WebDriver;


$botman = resolve('botman');

$dialogflow = ApiAi::create('597431ae8eed4f9d9ded7fcfdaf9346a ')->listenForAction();

$botman->middleware->received($dialogflow);

$botman->group(['middleware' => $dialogflow], function (BotMan $botman) {

    $botman->hears('input.welcome',BotManController::class.'@nlpDefaultReply');

    $botman->hears('input.unknown', BotManController::class.'@nlpDefaultReply');

    $botman->hears('saber_clima', BotManController::class.'@nlpDefaultReply');

    $botman->hears('peticion', BotManController::class.'@solicitudInicial');

});

$botman->on('widgetOpened', function(string $payload, BotMan $bot){

    Log::info('botman route: heard opened event');
    $bot->reply("EVENTO OPENED  :".$payload);
});

$botman->hears('Start conversation', BotManController::class.'@startConversation');


$botman->hears('database', function (BotMan $bot) {

    $results = DB::select('select * from conversacion where idconversacion = :idconversacion', ['idconversacion' => 1]);

    foreach($results as $post){
        $bot->reply($post->texto);
    }
});

$botman->hears('log', function (BotMan $bot) {
    Log::info('Log ruta');
    Log::debug('log ruta');

        $bot->reply('paso por log');

});



